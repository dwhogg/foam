
Foam
===========================

Deconvolution of noisy spectra.

People
-------

- Ross Fadely (NYU)
- David W. Hogg (NYU)


License
-------

Copyright 2013 the authors.

**foam** is free software licensed under the *MIT License*.  For
details, see the
`LICENSE file <https://raw.github.com/rossfadely/foam/master/LICENSE.rst>`_.
